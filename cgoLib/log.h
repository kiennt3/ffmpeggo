/* log.c */
/*
Mpeg's log library usage

#include <libavutil/log.h>
 / / Set the log level
av_log_set_level(AV_LOG_DEBUG)
 // print the log
av_log(NULL, AV_LOG_INFO,"...%s\n",op)

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libavutil/log.h>

/ / Define the function of the output log, leave it to the user to achieve
extern void Ffmpeglog(int , char*);

static void log_callback(void *avcl, int level, const char *fmt, va_list vl)
{
    (void) avcl;
    char log[1024] = {0};
    // int vsnprintf(buffer,bufsize,fmt, argptr) , va_list is a pointer to a variable argument, related methods are: va_start(), type va_atg(va_list, type), va_end()
    int n = vsnprintf(log, 1024, fmt, vl);
    if (n > 0 && log[n - 1] == '\n')
        log[n - 1] = 0;
    if (strlen(log) == 0)
        return;
    Ffmpeglog(level, log);
}

void set_log_callback()
{
    // Register the log callback function for the av decoder
    av_log_set_callback(log_callback);
}

